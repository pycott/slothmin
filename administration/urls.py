# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^actions/$', views.RunActionsView.as_view(), name='actions'),
]
