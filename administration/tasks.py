# -*- coding: utf-8 -*-
import logging

from celery import Task
from django.contrib.auth.models import User
import redis

from administration.models import Command, CeleryTaskResults
from servers.models import Server

logger = logging.getLogger(__name__)


class AsyncTaskMixin(object):
    abstract = True
    default_retry_delay = 5

    TASK_RUN = 'RUN'
    TASK_RETRY = 'retry'
    TASK_FAIL = 'fail'
    TASK_SUCCESS = 'success'

    def __init__(self):
        self.redis = redis.Redis()

    def set_channel(self, channel_name):
        self.channel = channel_name

    @classmethod
    def start(cls, *args, **kwargs):
        task = cls()
        task._prepare_data(*args, **kwargs)
        result = task.delay(*args, **kwargs)
        task.publish_result(result)

    def _prepare_data(self, *args, **kwargs):
        self.user_pk = kwargs.pop('user_pk')
        self.set_channel(self.user_pk)

    def publish_result(self, result):
        CeleryTaskResults.objects.create(result_id=result.id, user=User.objects.get(pk=self.user_pk))
        self.redis.publish(self.channel, '{}:{}'.format(result.id, self.TASK_RETRY))

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        logger.info('{} task in after return'.format(self.__class__))
        super(AsyncTaskMixin, self).after_return(status, retval, task_id, args, kwargs, einfo)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.info('{} task failure'.format(self.__class__))
        self._prepare_data(*args, **kwargs)
        self.redis.publish(self.channel, '{}:{}'.format(task_id, self.TASK_RETRY))
        super(AsyncTaskMixin, self).on_failure(exc, task_id, args, kwargs, einfo)

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        logger.info('{} task retry'.format(self.__class__))
        self._prepare_data(*args, **kwargs)
        self.redis.publish(self.channel, '{}:{}'.format(task_id, self.TASK_FAIL))
        super(AsyncTaskMixin, self).on_retry(exc, task_id, args, kwargs, einfo)

    def on_success(self, retval, task_id, args, kwargs):
        logger.info('{} task success'.format(self.__class__))
        self._prepare_data(*args, **kwargs)
        self.redis.publish(self.channel, '{}:{}'.format(task_id, self.TASK_SUCCESS))
        super(AsyncTaskMixin, self).on_success(retval, task_id, args, kwargs)


class RunTask(AsyncTaskMixin, Task):
    def run(self, servers, commands, *args, **kwargs):
        print("run commands {} for servers {}".format(commands, servers))
        commands = Command.objects.filter(id__in=commands)
        for server in Server.objects.filter(id__in=servers):
            commands.run(server)
