# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from django.views.generic import FormView

from administration.forms import RunActionsForm
from administration.tasks import RunTask

from common.mixins import BaseMixin


class RunActionsView(BaseMixin, FormView):
    title = 'администрирование: действия'
    template_name = 'administration/run_actions_form.html'
    form_class = RunActionsForm

    def get_success_url(self):
        return reverse('administration:actions')

    def get_context_data(self, **kwargs):
        context = super(RunActionsView, self).get_context_data(**kwargs)
        context['form_action'] = reverse('administration:actions')
        return context

    def form_valid(self, form):
        servers_ids = [server.id for server in form.cleaned_data['servers']]
        commands_ids = [command.id for command in form.cleaned_data['commands']]
        RunTask.start(servers_ids, commands_ids, user_pk=self.request.user.pk)
        return super(RunActionsView, self).form_valid(form)
