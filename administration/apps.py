# -*- coding: utf-8 -*-
from django.apps import AppConfig
from administration.models import Command


class AdministrationConfig(AppConfig):
    name = 'administration'

    def ready(self):
        Command.objects.actualize()
        import administration.signals.handlers
