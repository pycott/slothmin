# -*- coding: utf-8 -*-
from django import forms
from administration.models import Command
from servers.models import Server


class RunActionsForm(forms.Form):
    commands = forms.ModelMultipleChoiceField(
        queryset=Command.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        label="Команды"
    )
    servers = forms.ModelMultipleChoiceField(
        queryset=Server.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        label="Сервера"
    )
