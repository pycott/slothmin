# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models, ProgrammingError
import pkgutil
import importlib
import inspect
import logging
from actions import commands


logger = logging.getLogger(__name__)

class CommandQueryset(models.QuerySet):
    def run(self, *args, **kwargs):
        for command in self:
            command.run(*args, **kwargs)


class CommandManager(models.Manager):
    def get_queryset(self):
        return CommandQueryset(self.model, using=self._db)

    def actualize(self):
        if not self.is_table_exists():
            logger.info('table for commands does not exists')
            return
        actually_commands = []
        for new_command in self._find_commands():
            command, created = self.get_or_create(verbose_name=new_command.verbose_name, name=new_command.__name__,
                        module=new_command.__module__)
            actually_commands.append(command)
        removed_ids = [elem.id for elem in set(self.all()).difference(set(actually_commands))]
        if removed_ids:
            self.filter(id__in=removed_ids).delete()

    def _find_commands(self):
        commands_dir = pkgutil.os.path.join(settings.BASE_DIR, 'actions', 'commands')
        new_commands = []
        for module_loader, name, is_pkg in pkgutil.iter_modules([commands_dir]):
            if not is_pkg:
                new_module = importlib.import_module(commands.__name__ + '.' + name)
                cls = inspect.getmembers(new_module, inspect.isclass)[-1][1]
                new_commands.append(cls)
            else:
                raise RuntimeError('package importer is not implemented!')
        return new_commands

    def is_table_exists(self):
        try:
            Command.objects.count()
        except ProgrammingError:
            return False
        return True


class Command(models.Model):
    verbose_name = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    module = models.CharField(max_length=255)

    objects = CommandManager()

    def run(self, *args, **kwargs):
        module = importlib.import_module(self.module)
        getattr(module, self.name)(*args, **kwargs)

    def __unicode__(self):
        return self.verbose_name

    class Meta:
        unique_together = (("name", "module"),)
        app_label = 'administration'


class CeleryTaskResults(models.Model):
    result_id = models.CharField(max_length=255)
    user = models.ForeignKey(User)
    active = models.BooleanField(default=True)
