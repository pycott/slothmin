# coding=utf-8
from __future__ import unicode_literals
from django.conf import settings

from django.dispatch import receiver
from django.db.models.signals import post_save
import redis
from administration.models import CeleryTaskResults


@receiver(post_save, sender=CeleryTaskResults)
def send_to_redis(sender, instance, created, raw, **kwargs):
    if created:
        redis_conn = redis.StrictRedis(
            host = settings.REDIS['host'],
            port = settings.REDIS['port'],
            db = settings.REDIS['tasks_tornado_push_db']
        )
        redis_conn.publish(instance.user.username, instance.result_id)
