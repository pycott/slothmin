import os
import djcelery

djcelery.setup_loader()


if os.environ.get('ENVIRONMENT') == 'dev':
    from slothmin.develop_settings import *
else:
    from slothmin.production_settings import *
try:
    from slothmin.local_settings import *
except ImportError:
    pass

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '2go*7wxk03lh9!08(e3x855*g)ug!!$!6xz96gt2st&k^v1e06'

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = PRE_INSTALLED_APPS + (
    # system
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # other
    'bootstrap3',
    'djcelery',

    # slothmin
    'servers',
    'administration',
    'ci',
    'async_messenger',

) + POST_INSTALLED_APPS

MIDDLEWARE_CLASSES += (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'common.middlewares.RequireLoginMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    'common.context_processors.common_info',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
    os.path.join(BASE_DIR, 'servers/templates/'),
    os.path.join(BASE_DIR, 'ci/templates/'),
    os.path.join(BASE_DIR, 'common/templates/'),
)

ROOT_URLCONF = 'slothmin.urls'

WSGI_APPLICATION = 'slothmin.wsgi.application'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            "stream": "ext://sys.stdout"
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': LOG_LEVEL,
        },
        'django.request': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        },
        'django.db': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        "actions": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "administration": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "async_messenger": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "system_web_socket": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "ci": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "common": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "servers": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "slothmin": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
    },
}

# Internationalization
LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

DATE_FORMAT = 'd.m.Y'
DATETIME_FORMAT = 'd.m.Y - H:i:s  O'
TIME_FORMAT = 'H:i:s'

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'common', 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'

#middleware login_required
LOGIN_REQUIRED_URLS = (
    r'(.*)$',
)

LOGIN_REQUIRED_URLS_EXCEPTIONS = (
    r'/login/(.*)$',
)

#redis
REDIS = {
    'host': 'localhost',
    'port': 6379,
    'session_db': 0,
    'celery_tasks_db': 1,
    'tasks_tornado_push_db': 2,
}

#celery
BROKER_URL = 'redis://{host}:{port}/{celery_tasks_db}'.format(**REDIS)
CELERY_RESULT_BACKEND = 'redis://{host}:{port}/{celery_tasks_db}'.format(**REDIS)
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"

#login
LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/'
SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_HOST = REDIS['host']
SESSION_REDIS_PORT = REDIS['port']
SESSION_REDIS_DB = REDIS['session_db']
SESSION_REDIS_PREFIX = 'session'

#async messenger
ASYNC_MESSENGER_PORT = 8181
ASYNC_MESSENGER_SYSTEM_WS_URL = '/system_ws'
ASYNC_MESSENGER_TORNADO_SETTINGS = {
    "debug": DEBUG,
    "xsrf_cookies": True,
}

#context processor common_info
COMMON_INFO = {
    'system_ws_url': 'ws://127.0.0.1:{}{}'.format(ASYNC_MESSENGER_PORT, ASYNC_MESSENGER_SYSTEM_WS_URL)
}
