# coding=utf-8
from __future__ import unicode_literals


DEBUG = False

ALLOWED_HOSTS = ['*']

PRE_INSTALLED_APPS = ()
POST_INSTALLED_APPS = ()

MIDDLEWARE_CLASSES = ()

TEMPLATE_CONTEXT_PROCESSORS = ()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'slothmin_db',
        'USER': 'slothmin',
        'PASSWORD': '123123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

LOG_LEVEL = 'INFO'
