# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('administration:actions'), permanent=True), name='go_main'),
    url(r'^servers/', include('servers.urls', namespace='servers', app_name='servers')),
    url(r'^administration/', include('administration.urls', namespace='administration', app_name='administration')),
    url(r'^ci/', include('ci.urls', namespace='ci', app_name='ci')),

    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'simple_form.html'},
        name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': "login"}, name="logout"),
]
