# -*- coding: utf-8 -*-
INTERNAL_IPS = '127.0.0.1'

DEBUG = True
ALLOWED_HOSTS = ['*']

PRE_INSTALLED_APPS = ()

POST_INSTALLED_APPS = ()

# в случае попытки сохранения относительного
# времени в бд джанго кинет исключение
import warnings
warnings.filterwarnings(
        'error', r"DateTimeField .* received a naive datetime",
        RuntimeWarning, r'django\.db\.models\.fields')

MIDDLEWARE_CLASSES = ()

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.debug",
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'slothmin_db',
        'USER': 'slothmin',
        'PASSWORD': '123123',
        'HOST': '',
        'PORT': '',
    }
}

LOG_LEVEL = 'DEBUG'
