var SystemWS = function(url){
    var self = this;
    self.ws = new WebSocket(url);
    console.log('ws: CONNECT to server');

    self.ws.onopen = function (evt) {
        console.log('ws: opened');
        //self.ws.send(JSON.stringify({
        //    'status': 'init',
        //    'data': 'hello'
        //}));
    };

    self.ws.onmessage = function(resp) {
        resp = JSON.parse(resp.data);
        var status = resp.status,
            data = resp.data;
        if (status == 'redis_disconnect'){
            console.log('ws: redis_disconnect. '+data.message);
        }
        else if (status == 'unauthorized'){
            console.log('ws: unauthorized. '+data.message);
        }
    };

    return self.ws
};