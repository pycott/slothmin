# -*- coding: utf-8 -*-
from django import forms
from servers.models import Server


class EditServerForm(forms.ModelForm):
    def save(self, commit=True):
        if not self.instance.name:
            self.instance.name = self.instance.host
        return super(EditServerForm, self).save()

    class Meta:
        model = Server
        fields = ('name', 'sudo_user', 'host', 'port',)
