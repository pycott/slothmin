# coding=utf-8
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models


class Server(models.Model):
    name = models.CharField(max_length=55, blank=True, null=True)
    sudo_user = models.CharField(max_length=55)
    host = models.GenericIPAddressField()
    port = models.SmallIntegerField(default=22)

    @property
    def delete_url(self):
        return reverse('servers:delete', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.name
