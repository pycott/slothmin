# coding=utf-8
from __future__ import unicode_literals
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView

from common.mixins import BaseMixin
from servers.forms import EditServerForm
from servers.models import Server


class List(BaseMixin, ListView):
    model = Server
    title = 'сервера: cписок'


class Create(BaseMixin, CreateView):
    model = Server
    form_class = EditServerForm
    template_name = 'simple_form.html'
    title = 'сервера: добавить'

    def get_success_url(self):
        return reverse('servers:list')

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['form_action'] = reverse('servers:add')
        return context


class Delete(DeleteView):
    model = Server
    success_url = reverse_lazy('servers:list')
