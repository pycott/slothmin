# coding=utf-8
from __future__ import unicode_literals

import time

from actions import Command


class DebugCommand(Command):
    verbose_name = 'just time sleep for debug'

    def run(self, *args, **kwargs):
        for i in range(10):
            print(i)
            time.sleep(1)
