# -*- coding: utf-8 -*-
import os

from fabric.contrib.files import append
from actions import Command


class SshCopyId(Command):
    verbose_name = 'скопировать ssh ключ на сервер'

    def run(self, *args, **kwargs):
        self.ssh_copy_id(kwargs.get('ssh_key', None))

    def ssh_copy_id(self, ssh_key):
        ssh_key = ssh_key or self.find_ssh_key_pub()
        with open(ssh_key) as f:
            text = f.read()
            append('~/.ssh/authorized_keys', text)

    def find_ssh_key_pub(self):
        ssh_dir = os.path.join(os.path.expanduser('~'), '.ssh')
        if not os.path.exists(ssh_dir):
            raise RuntimeWarning('{} directory not found'.format(ssh_dir))
        ssh_key_pub = os.path.join(ssh_dir, 'id_rsa.pub')
        if not os.path.exists(ssh_key_pub):
            raise RuntimeWarning('{} not found'.format(ssh_key_pub))
        return ssh_key_pub
