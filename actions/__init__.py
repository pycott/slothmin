# -*- coding: utf-8 -*-
from fabric.state import env


class Command(object):
    verbose_name = ''

    def __init__(self, *args, **kwargs):
        self.setup_host(kwargs.get('server', None))
        self.run(*args, **kwargs)

    def setup_host(self, server):
        if server:
            env.host_string = '{user}@{host}:{port}'.format(
                user=server.sudo_user,
                host=server.host,
                port=server.port
            )

    def run(self, *args, **kwargs):
        raise RuntimeError('run method not implemented')
