from django.conf import settings
from async_messenger import controllers

url_patterns = [
    (r"{}".format(settings.ASYNC_MESSENGER_SYSTEM_WS_URL), controllers.SystemWebSocket),
]
