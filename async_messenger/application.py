# coding=utf-8
from django.conf import settings

import tornado.web
from async_messenger.urls import url_patterns


application = tornado.web.Application(url_patterns, **settings.ASYNC_MESSENGER_TORNADO_SETTINGS)
