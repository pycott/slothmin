class Protocol(object):
    unauthorized = {
        'status': 'unauthorized',
        'data': {
            'message': 'not authorized'
        }
    }

    redis_disconnect = {
        'status': 'redis_disconnect',
        'data': {
            'message': 'The connection terminated due to a Redis server error.'
        }
    }
