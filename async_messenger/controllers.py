# coding=utf-8
import logging
from celery.result import AsyncResult

from django.contrib.auth.models import User
from django.utils.importlib import import_module
import tornado.websocket
import tornado.gen
from django.conf import settings
import tornadoredis

from administration.models import CeleryTaskResults
from async_messenger.protocol import Protocol

session_engine = import_module(settings.SESSION_ENGINE)


class SystemWebSocket(tornado.websocket.WebSocketHandler):
    logger = logging.getLogger("system_web_socket")
    connections = {}

    def __init__(self, application, request, **kwargs):
        super(SystemWebSocket, self).__init__(application, request, **kwargs)
        self.tasks = set()

    def check_origin(self, origin):
        return True

    def open(self):
        if self.authenticate():
            self.logger.debug('opened ws from {}'.format(self.request.remote_ip))
            self.add_user()
            self.check_tasks()
            self.listen()
        else:
            self.write_message(Protocol.unauthorized)
            self.close()

    def on_message(self, msg):
        if msg.kind == 'message':
            self.tasks.add(CeleryTaskResults.objects.get(result_id=msg.body))
        elif msg.kind == 'disconnect':
            self.write_message(Protocol.redis_disconnect)
            self.close()
        elif msg.kind == 'subscribe':
            pass
        else:
            raise NotImplementedError('handler for kind "{}" not implemented'.format(msg.kind))

    @tornado.gen.engine
    def listen(self):
        self.client = tornadoredis.Client()
        self.client.connect()
        yield tornado.gen.Task(self.client.subscribe, self.user.username)
        self.client.listen(self.on_message)

    def check_tasks(self):
        self.tasks.update(CeleryTaskResults.objects.filter(user=self.user, active=True))
        for task in self.tasks:
            result = AsyncResult(task.result_id)
            if result.ready():
                task.active = False
                task.save()

    def on_close(self):
        self.remove_user()

    def authenticate(self):
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)
        session = session_engine.SessionStore(session_key)
        try:
            self.user_id = session["_auth_user_id"]
            self.user = User.objects.get(id=self.user_id)
            self.logger.debug('authorized user: {}'.format(self.user.username))
        except (KeyError, User.DoesNotExist):
            self.logger.debug('unauthorized user')
            return False
        return True

    def add_user(self):
        SystemWebSocket.connections[self.user.username] = self

    def remove_user(self):
        if hasattr(self, 'username') and self.user.username in SystemWebSocket.connections:
            self.logger.debug('remove user: {}'.format(self.user.username))
            SystemWebSocket.connections.pop(self.user.username)
