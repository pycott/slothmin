import signal
import time
import logging

import tornado.httpserver
import tornado.ioloop
from django.core.management.base import BaseCommand
from django.conf import settings

from async_messenger.application import application


class Command(BaseCommand):
    help = 'Starts the Tornado application for message handling.'

    def sig_handler(self, sig, frame):
        tornado.ioloop.IOLoop.instance().add_callback(self.shutdown)

    def shutdown(self):
        self.http_server.stop()
        io_loop = tornado.ioloop.IOLoop.instance()
        io_loop.add_timeout(time.time() + 2, io_loop.stop)

    def handle(self, *args, **options):
        self.http_server = tornado.httpserver.HTTPServer(application)
        self.http_server.listen(settings.ASYNC_MESSENGER_PORT, address="127.0.0.1")
        logging.getLogger(__name__).info("server start")
        signal.signal(signal.SIGTERM, self.sig_handler)
        signal.signal(signal.SIGINT, self.sig_handler)
        tornado.ioloop.IOLoop.instance().start()
