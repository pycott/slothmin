# coding=utf-8
from __future__ import unicode_literals

from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, DeleteView

from ci.forms import EditCommitCheckerForm
from ci.models import ProjectChecker
from common.mixins import BaseMixin


class Create(BaseMixin, CreateView):
    model = ProjectChecker
    form_class = EditCommitCheckerForm
    template_name = 'simple_form.html'
    title = 'Continuous integration: добавить проверку коммитов'
    success_url = reverse_lazy('ci:list')

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['form_action'] = reverse('ci:add')
        return context


class List(BaseMixin, ListView):
    model = ProjectChecker
    title = 'Continuous integration: cписок коммит чекеров'


class Delete(DeleteView):
    model = ProjectChecker
    success_url = reverse_lazy('ci:list')
