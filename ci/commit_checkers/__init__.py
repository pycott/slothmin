class CommitChecker(object):
    base_uri = ''

    def __init__(self, domain, project, repo, branch, credentials, *args, **kwargs):
        self.url = self.base_uri.format(domain=domain, project=project, repo=repo, branch=branch)
        self._credentials = credentials

    def _local_last_commit(self):
        raise Exception('method not implemented')

    def _remote_last_commit(self):
        raise Exception('method not implemented')

    @property
    def is_updated(self):
        return self._local_last_commit() != self._remote_last_commit()
