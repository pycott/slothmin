# coding=utf-8
from __future__ import unicode_literals
import requests
from ci.commit_checkers import CommitChecker


class StashCommitChecker(CommitChecker):
    base_uri = 'http://{domain}/rest/api/1.0/projects/{project}/repos/{repo}/commits/?until={branch}'

    def _remote_last_commit(self):
        resp = requests.get(self.url,  auth=self._credentials)
        commits = resp.json().get('values')
        last_commit = commits[0] if commits else {}
        return last_commit
