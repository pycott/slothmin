# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^list/$', views.List.as_view(), name='list'),
    url(r'^add/$', views.Create.as_view(), name='add'),
    url(r'^delete/(?P<pk>[\d]+)/$', views.Delete.as_view(), name='delete'),
]
