# coding=utf-8
from __future__ import unicode_literals
from django import forms
from ci.models import ProjectChecker


class EditCommitCheckerForm(forms.ModelForm):
    class Meta:
        model = ProjectChecker
        fields = ('domain', 'project', 'repo', 'branch',)
