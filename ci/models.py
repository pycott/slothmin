from django.core.urlresolvers import reverse
from django.db import models


class ProjectChecker(models.Model):
    domain = models.CharField(max_length=255)
    project = models.CharField(max_length=255)
    repo = models.CharField(max_length=255)
    branch = models.CharField(max_length=255)

    @property
    def delete_url(self):
        return reverse('servers:delete', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.name


class Commit(models.Model):
    name = models.CharField(max_length=255)
    checker = models.ForeignKey('ProjectChecker')
