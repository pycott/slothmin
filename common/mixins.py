# -*- coding: utf-8 -*-


class BaseMixin(object):
    css = []
    js = []
    title = ''

    def get_context_data(self, **kwargs):
        context = super(BaseMixin, self).get_context_data(**kwargs)
        context['additional_scripts'] = self.js
        context['additional_css'] = self.css
        context['title'] = self.title
        return context
